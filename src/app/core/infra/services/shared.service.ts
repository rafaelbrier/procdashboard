

export class SharedService {

    static convertTimeToMilliseconds(time: Date) {
        let hrs = time.getHours();
        let min = time.getMinutes();
        let sec = time.getSeconds();
        return((hrs*60*60+min*60+sec)*1000);
    }

    static convertMillisecondsToDigitalClock(ms: any) {
        let hours = Math.floor(ms / 3600000); // 1 Hour = 36000 Milliseconds
        let minutes = Math.floor((ms % 3600000) / 60000); // 1 Minutes = 60000 Milliseconds
        let seconds = Math.floor(((ms % 360000) % 60000) / 1000); // 1 Second = 1000 Milliseconds

        let hoursStr = hours.toString();
        let minutesStr = minutes.toString();
        let secondsStr = seconds.toString();
        if(hours < 10) {
            hoursStr = '0' + hoursStr;
        }
        if(minutes < 10) {
            minutesStr = '0' + minutesStr;
        }
        if(seconds < 10) {
            secondsStr = '0' + secondsStr;
        }
        return hoursStr + ":" + minutesStr + ":" + secondsStr;
    }

    static buildQueryParams(params: any): string {
        let queryParams: string = '?';
    
        if(params) {
          
          for (let property in params) {
            if(params[property]) {
                queryParams += `&${[property]}=${params[property]}`;
            }
          }
        }
    
        return queryParams;
      }

}
