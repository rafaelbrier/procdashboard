import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { HttpConnectionBuilder } from '../http/http-connection.builder';
import { Subscription } from 'rxjs';
import { ProcLogError } from '../../model/error.model';
import { EP } from './endPoints';

@Injectable()
export class ErrorService {

    constructor(private http: HttpClient){
    }

    public findErrorsOfExec(execId: string, handlerSuccess: (value: ProcLogError[]) => void, handlerError: (value: any) => void): Subscription {
        return new HttpConnectionBuilder<any>(this.http)
            .addEndPoint(`${EP.procLogExec}/${execId}${EP.errors}`)
            .addHandlerSucess(handlerSuccess)
            .addHandlerError(handlerError)
            .buildGet();
    }
}