import { Chart } from 'angular-highcharts';
import { SharedService } from './shared.service';
import { ProcChartData, GraphChart } from '../../model/chart.model';

export class ChartService {

    static plotModalGraphs(chartData: ProcChartData[]): GraphChart {
        let dataArea: any[] = [];
        let dataLine: any[] = [];
        let dataError: any[] = [];
        let dataNumExec: any[] = [];
        let graphCharts: GraphChart = {};
        chartData.forEach(element => {
            let dateTime = new Date(element.dataInicio);
            let date = new Date(dateTime.getFullYear(), dateTime.getMonth(), dateTime.getDate() + 1);
            let elementStartDate = date.getTime();

            dataArea.push([elementStartDate, element.tempoExecucao]);
            dataLine.push([elementStartDate, element.tempoEstimado]);
            dataError.push([elementStartDate, element.numeroErros]);
            dataNumExec.push([elementStartDate, element.numeroExecucao]);

        });
        let chartDatalenght = chartData.length;
        let numExec: number | string = chartData.map(e => { return parseInt(e.numeroExecucao, 10) })
            .reduce((a, b) => a + b, 0);
        let numErros: number | string = chartData.map(e => { return parseInt(e.numeroErros, 10) })
            .reduce((a, b) => a + b, 0);
        let tempoMedio: number | string = chartData.map(e => { return parseInt(e.tempoExecucao, 10) })
            .reduce((a, b) => a + b, 0);

        let dayTime = 24 * 3600 * 1000;
        let tickInterval = numExec > 15 ? (8 * dayTime) : dayTime / 2;
        let disponibilidade: number = (1 - (numErros / numExec)) * 100;
        disponibilidade = parseFloat(disponibilidade.toFixed(2));
        tempoMedio = SharedService.convertMillisecondsToDigitalClock(Math.round(tempoMedio / chartDatalenght));

        graphCharts.chartExec = this.plotChartExec(dataArea, dataLine, tickInterval);
        graphCharts.chartError = this.plotChartError(dataError, dataNumExec, tickInterval);
        graphCharts.tempoMedio = tempoMedio;
        graphCharts.disponibilidade = disponibilidade;
        graphCharts.numExec = numExec;

        return graphCharts;
    }

    static plotChartExec(dataArea: any[], dataLine: any[], tickInterval: number) {

        return new Chart({
            chart: {
                height: 375,
                zoomType: 'x'
            },
            credits: {
                enabled: false
            },
            time: {
                timezoneOffset: 3 * 60 //GMT -3hrs
            },
            title: {
                text: 'Tempo de Execução por Dia'
            },
            xAxis: {
                tickInterval: tickInterval,
                type: 'datetime',
                labels: {
                    style: { fontSize: '12' },
                    format: "{value: %e %b %Y}",
                    useHtml: true
                },
            },
            yAxis: {
                title: {
                    style: { fontSize: '14' },
                    text: 'Tempo de Execução'
                },
                labels: {
                    style: { fontSize: '12' },
                    formatter: function () {
                        return SharedService.convertMillisecondsToDigitalClock(this.value)
                    }
                }
            },
            series: [{
                name: 'Tempo de Execução',
                data: dataArea,
                tooltip: {
                    headerFormat: '',
                    pointFormat: `Tempo de Execução (ms): <b>{point.y:,.0f}</b>
                    <br/>Executado em: {point.x: %e %b %Y - %H:%M:%S}`
                },
                marker: {
                    enabled: dataArea.length < 15 ? true  : false,
                    symbol: 'circle',
                    radius: 5
                },
                type: 'area'
            }, {
                name: 'Tempo Previsto',
                data: dataLine,
                enableMouseTracking: false,
                marker: {
                    enabled: dataLine.length < 15 ? true  : false,
                    symbol: 'triangle',
                    radius: 5
                },
                type: 'line'
            }]
        });
    }

    static plotChartError(dataError: any[], dataExec: any[], tickInterval: number) {

        return new Chart({
            chart: {
                height: 375,
                zoomType: 'x'
            },
            credits: {
                enabled: false
            },
            time: {
                timezoneOffset: 3 * 60 //GMT -3hrs
            },
            title: {
                text: 'Execuções por Dia'
            },
            xAxis: {
                tickInterval: tickInterval,
                type: 'datetime',
                labels: {
                    style: { fontSize: '12' },
                    format: "{value: %e %b %Y}",
                    useHtml: true
                },
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    style: { fontSize: '14' },
                    text: 'Quantidade'
                },
                labels: {
                    style: { fontSize: '12' },
                    formatter: function () {
                        return this.value + '';
                    }
                }
            },
            series: [{
                name: 'Quantidade de Execuções',
                data: dataExec,
                color: 'darkblue',
                tooltip: {
                    headerFormat: '',
                    pointFormat: `Execuções: <b>{point.y:,.0f}</b>
                        <br/>Data: {point.x: %e %b %Y}`
                },
                marker: {
                    enabled: dataExec.length < 15 ? true  : false,
                    radius: 7,
                    symbol: 'triangle'
                },
                type: 'line'
            },{
                name: 'Quantidade de Execuções com Erro',
                data: dataError,
                color: 'red',
              //  maxPointWidth: 15,
                tooltip: {
                    headerFormat: '',
                    pointFormat: `Erros: <b>{point.y:,.0f}</b>
                    <br/>Data: {point.x: %e %b %Y}`
                },
                marker: {
                    enabled: dataError.length < 15 ? true  : false,
                    radius: 3,
                    symbol: 'circle'
                },
                //minPointLength: 5,
                type: 'line'
            }]
        });
    }

    // static plotChartError(dataBarError: any[], dataBarNumExec: any[], tickInterval: number) {
    //     return new Chart({
    //         chart: {
    //             height: 375,
    //             zoomType: 'x'
    //         },
    //         credits: {
    //             enabled: false
    //         },
    //         time: {
    //             timezoneOffset: 3 * 60 //GMT -3hrs
    //         },
    //         title: {
    //             text: 'Quantidade de Execuções com Erros por Dia'
    //         },
    //         xAxis: {
    //             tickInterval: tickInterval,
    //             type: 'datetime',
    //             labels: {
    //                 style: {fontSize: '12'},
    //                 format: '{value:%e %b %Y}'
    //             },
    //         },
    //         yAxis: {
    //             allowDecimals: false,
    //             title: {
    //                 style: {fontSize: '14'},
    //                 text: 'Quantidade'
    //             },
    //             labels: {
    //                 style: {fontSize: '12'},
    //                 formatter: function () {
    //                     return this.value + '';
    //                 }
    //             }
    //         },
    //         plotOptions: {
    //             column: {
    //                 grouping: false,
    //                 shadow: false,
    //                 borderWidth: 0
    //             }
    //         },
    //         series: [{
    //             name: 'Quantidade de Execuções',
    //             data: dataBarNumExec,
    //             color: 'rgba(126,86,134,.9)',
    //             maxPointWidth: 50,
    //             pointPadding: 0.3,
    //             pointPlacement: -0.2,
    //             tooltip: {
    //                 headerFormat: '',
    //                 pointFormat: `Qtd de Execuções: <b>{point.y:,.0f}</b>
    //                 <br/>Data: {point.x: %e %b %Y}`
    //             },
    //             marker: {
    //                 enabled: true,
    //                 symbol: 'circle',
    //                 radius: 5
    //             },
    //             minPointLength: 5,
    //             type: 'column'
    //         },
    //         {
    //             name: 'Quantidade de Erros',
    //             data: dataBarError,
    //             color: 'red',
    //             maxPointWidth: 30,
    //             pointPadding: 0.4,
    //             pointPlacement: -0.2,
    //             tooltip: {
    //                 headerFormat: '',
    //                 pointFormat: `Qtd de Erros: <b>{point.y:,.0f}</b>
    //                 <br/>Data: {point.x: %e %b %Y}`
    //             },
    //             marker: {
    //                 enabled: true,
    //                 symbol: 'circle',
    //                 radius: 5
    //             },
    //             minPointLength: 5,
    //             type: 'column'
    //         },

    //     ]
    //     });
    // }

    // plotPieGraph(chartData: ProcChartData[]) {
    //     let numErros: number = 0;
    //     let totalNumExec: number = chartData.length;

    //     chartData.forEach((element) => {
    //          numErros = numErros + parseInt(element.numeroErros,10);          
    //     });

    //     return new Chart({
    //         credits: {
    //             enabled: false
    //         },
    //         title: {
    //             text: 'Disponibilidade',
    //             style: { fontSize: '25' }
    //         },
    //         series: [{
    //             name: 'Porcentagem de Erros',
    //             type: 'pie',
    //             data: [{
    //                 name: 'Execuções sem Erros',
    //                 y: 1 - numErros / totalNumExec,
    //                 dataLabels: {
    //                     format: `<b>{point.name}</b>: {point.percentage:.1f}% (${totalNumExec - numErros})`
    //                 },
    //                 sliced: true,
    //                 selected: true
    //             }, {
    //                 name: 'Execuções com Erros',
    //                 dataLabels: {
    //                     format: `<b>{point.name}</b>: {point.percentage:.1f}% (${numErros})`
    //                 },
    //                 y: numErros / totalNumExec
    //             }],
    //             colors: ['blue', 'red'],
    //             colorByPoint: true,
    //             allowPointSelect: true,
    //             cursor: 'pointer',
    //             dataLabels: {
    //                 style: { fontSize: '15' },
    //                 enabled: true,
    //             },
    //             tooltip: {
    //                 pointFormat: `{series.name}: <b>{point.percentage:.1f}%</b>`
    //             },
    //             marker: {
    //                 enabled: false,
    //             }
    //         }]
    //     });
    // }

}
