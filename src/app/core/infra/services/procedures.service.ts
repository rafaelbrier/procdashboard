import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { take } from 'rxjs/operators';
import { SharedService } from './shared.service';
import { ProcAvailable } from '../../model/procedure.model';
import { EP } from './endPoints';

@Injectable()
export class ProceduresService {

    constructor(private http: HttpClient){}

    findMalhasNome() {
        return this.http.get(`${EP.restBaseUrl}${EP.malhasNome}`).pipe(take(1));
    }

    findSistemas() {
        return this.http.get(`${EP.restBaseUrl}${EP.procAvailable}${EP.sistemas}`).pipe(take(1));
    }

    findAllMalhas() {
        return this.http.get(`${EP.restBaseUrl}${EP.allMalhas}`).pipe(take(1));
    }

    findAllLogsPageable(page: number = 0,
         size: number = 15,
         sort: String = "dataInicio",
         order: String = "desc", filtro: any) {

        return this.http.get(
            `${EP.restBaseUrl}${EP.procLogExec}${SharedService.buildQueryParams(filtro)}&page=${page}&size=${size}`
            ).pipe(take(1));
      }

      findAllProceduresPageable(page: number = 0,
        size: number = 200,
        sort: String = "criadoEm",
        order: String = "asc", filtro: any) {

       return this.http.get(
           `${EP.restBaseUrl}${EP.procAvailable}${SharedService.buildQueryParams(filtro)}&page=${page}&size=${size}&sort=${sort},${order}`
           ).pipe(take(1));
     }

     findAll() {
         return this.http.get(`${EP.restBaseUrl}${EP.procAvailable}${EP.all}`);
     }

     findChartData(procId: string) {
        return this.http.get(
            `${EP.restBaseUrl}${EP.procLogExec}${EP.chart}?procId=${procId}`
        ).pipe(take(1));
     }

     translateProcAvailable(proc: ProcAvailable) {
         let procTranslated: ProcAvailable = proc;
           if (procTranslated.criticidade === "M") {
            procTranslated.criticidade = "Média";
         } else if (procTranslated.criticidade === "A") {
            procTranslated.criticidade = "Alta";
         }  else if (procTranslated.criticidade === "B") {
            procTranslated.criticidade = "Baixa";
         } 
         if (procTranslated.fechamento === "S") {
            procTranslated.fechamento = "Sim";
         } else {
            procTranslated.fechamento = "Não";
         }
         if (procTranslated.periodicidade === "M") {
            procTranslated.periodicidade = "Mensal";
         } else if (procTranslated.periodicidade === "S") {
            procTranslated.periodicidade = "Semanal";
         } else {
            procTranslated.periodicidade = "Diário";
         }
         if(procTranslated.tempoMedio) {
             procTranslated.tempoMedioDigital
             = SharedService.convertMillisecondsToDigitalClock(procTranslated.tempoMedio);
         } else {
             procTranslated.tempoMedioDigital = null;
         }
         return procTranslated;
     }
}