import { formatDate } from '@angular/common';
import { ProcAvailable } from '../../model/procedure.model';

export class ExcelService {

    static convertToCSV(args) {
        let result, ctr, keys, columnDelimiter, lineDelimiter, data;

        data = args.data || null;
        if (data == null || !data.length) {
            return null;
        }

        columnDelimiter = args.columnDelimiter || ',';
        lineDelimiter = args.lineDelimiter || '\n';

        keys = Object.keys(data[0]);

        result = '';
        result += keys.join(columnDelimiter);
        result += lineDelimiter;

        data.forEach(function (item) {
            ctr = 0;
            keys.forEach(function (key) {
                if (ctr > 0) result += columnDelimiter;

                result += item[key];
                ctr++;
            });
            result += lineDelimiter;
        });

        return result;
    }

    static exportToCSVFile(data: any[], fileTitle: string) {
        let filename = fileTitle + '.csv' || 'export.csv';
        let csv = this.convertToCSV({
            data: data
        });
        if (csv == null)
            return;

        let blob = new Blob(["\ufeff" + csv], { type: "text/csv;charset=utf-8;" });

        if (navigator.msSaveBlob) {
            navigator.msSaveBlob(blob, filename)
        }
        else {
            let link = document.createElement("a");
            if (link.download !== undefined) {

                let url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }


    static formatProcToCSV(data: ProcAvailable[]): any[] {
        let dataCopy: any[] = [];
        const dateFormat = 'dd/MM/yyyy - HH:mm:ss';
        const dateLocale = 'en-US';
        data.forEach(element => {
            if(element.tempoMedio = '00:00:00') {
                element.tempoMedio = null;
            }
            dataCopy.push({
                'Nome': element.nome ? element.nome : '---',
                'Malha': element.malhaNome ? element.malhaNome: '---',
                'Banco': element.banco ? element.banco : '---',
                'Sistema': element.sistema ? element.sistema : '---',
                'Descrição': element.descricao ? element.descricao : '---',
                'Criticidade': element.criticidade ? element.criticidade : '---',
                'Fechamento': element.fechamento ? element.fechamento : '---',
                'Periodicidade': element.periodicidade ? element.periodicidade : '---',
                'Dia_de_Execução': element.diaInicio ? formatDate(element.diaInicio, dateFormat, dateLocale) : '---',
                'Hora_de_Execução': element.horaInicio ? formatDate(element.horaInicio, "HH'h'mm", dateLocale) : '---',
                'Tempo_Médio': element.tempoMedio ? element.tempoMedio : '---',
                'Tempo_Estimado': element.tempoEstimado ? formatDate(element.tempoEstimado, "HH:mm:ss", dateLocale) : '---',
                'Última_Execução': element.ultimaExecucao ? formatDate(element.ultimaExecucao, dateFormat, dateLocale) : '---',
                'Observação': element.observacao ? element.observacao : '---',
                'ID no Banco': element.procId,
                'Data_de_Criação': element.criadoEm ? formatDate(element.criadoEm, dateFormat, dateLocale) : '---'
            });
        });

        return dataCopy;
    }
}