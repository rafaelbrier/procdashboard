export const EP = {
    restBaseUrl: 'http://localhost:8080',
    procAvailable: '/procavailable',  
    all: '/all',
    malhasNome:'/malhas/nome',
    allMalhas: '/malhas',
    sistemas:'/sistemas',
    procLogExec: '/proclogexec',
    chart: '/chart',
    errors: '/errors'
}