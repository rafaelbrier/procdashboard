export interface ProcLogExec {
    //From Database ----
    logExecId?: string,
    dataInicio?: string  | Date,
    dataFim?: string  | Date,
    tempoExecucao?: string,
    status?: string,
    procAvailable?: ProcAvailable,
    //---------------------
    //For Client
    tempoMedio?: string,
    tempoMedioDigital?: string
}

export interface ProcAvailable {
    procId?: string,
    nome?: string,
    banco?: string,
    descricao?: string,
    malha?: Malha,
    malhaNome?: string
    sistema?: string,
    criticidade?: string,
    fechamento?: string,
    diaInicio?: string,
    horaInicio?: string,
    tempoEstimado?: string,
    ultimaExecucao?: string | Date,
    periodicidade?: string,
    observacao?: string,
    tempoMedio?: string,
    criadoEm?: string | Date,
     //For Client
    tempoMedioDigital?: string
}

export interface Malha {
    malhaId?: string,
    nome?: string,
    descricao?: string
}
