import { Chart } from 'angular-highcharts';

export interface ProcChartData {
    tempoExecucao?: string,
    dataInicio?: string | Date,
    numeroErros?: string,
    tempoEstimado?: string | number,
    numeroExecucao?: string
}

export interface GraphChart {
    chartExec?: Chart,
    chartError?: Chart,
    tempoMedio?: string,
    disponibilidade?: number,
    numExec?: number
}