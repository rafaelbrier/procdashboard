import { Input } from '@angular/core';

export class ModalModel {
    @Input()
    ref: any;

    isBoxLoading: boolean = false;
    boxLoadingError: boolean = false;
    elementCount: number = 0;

    constructor() { }

    close() {
        this.ref.dismiss();        
    }
}