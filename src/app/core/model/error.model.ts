export interface ProcLogError {
    logErrorId?: string,
    procId?: string,
    procLogExec?: string,   
    mensagem?: string,
    codigo?: string,
    severidade?: string,
    estado?: string,
    linha?: string,
    criadoEm?: string | Date ,
}
