import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProcLogExec } from 'src/app/core/model/procedure.model';
import { ProceduresService } from 'src/app/core/infra/services/procedures.service';
import { SharedService } from 'src/app/core/infra/services/shared.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-proclog',
  templateUrl: './proclog.page.html',
  styleUrls: ['./proclog.page.scss']
})
export class ProcLogPage implements OnInit, OnDestroy {

  //ModalsRef
  errorLogModalRef: NgbModalRef;
  infoLogModalRef: NgbModalRef;
  chartModalRef: NgbModalRef;
  errorModalData: ProcLogExec;
  infoModalData: ProcLogExec;
  chartModalData: ProcLogExec;

  isBoxLoading: boolean = false;
  boxLoadingError: boolean = false;
  lastUpdate: Date;
  procLogExecList: ProcLogExec[];
  procLogExecPagination: any;

  status: string = "Todos";
  statusOptions: any[] = [{ value: "Todos", info: "Todos" }, { value: "R", info: "Execução" },
  { value: "S", info: "Sucesso" }, { value: "E", info: "Error" }];

  malha: string = "Todos";
  malhaOptions: any[] = [{ value: "Todos", info: "Todos" }];

  sistema: string = "Todos";
  sistemaOptions: any[] = [{ value: "Todos", info: "Todos" }];

  //Pagination
  pages: number = 0;
  pagNumberOfPages: number;
  orderBy: string = "dataInicio";
  orderAscOrDesc: string = "desc";
  limit: number = 15;
  limitOptions: number[] = [15, 30, 45, 60];

  colTimerArr: any[] = [];
  autoUpdateTimer: any;
  enableAutoUpdate: boolean = false;

  filtro: any = {
    nameSearch: null,
    startDateSearch: null,
    malhaSearch: null,
    sistemaSearch: null,
    execStatusSearch: null
  }

  constructor(private proceduresService: ProceduresService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.startUpdater();
    this.populateSelects();
    this.findLogOfProceduresExec();
  }

  ngOnDestroy() {
    if (this.autoUpdateTimer) {
      clearInterval(this.autoUpdateTimer);
    }
    this.colTimerArr.forEach(element => {
      if (element) {
        clearInterval(element);
      }
    });
  }

  startUpdater() {
    this.autoUpdateTimer = setInterval(() => {
      if (this.enableAutoUpdate) {
        this.findLogOfProceduresExec();
      }
    }, 10000)
  }

  onPageChange(page: number) {
    this.pages = page - 1;
    this.findLogOfProceduresExec();
  }

  onDateSearchChange(dateSearchValue: string) {
    this.filtro.startDateSearch = dateSearchValue;
    this.findLogOfProceduresExec();
  }

  onNameSearchChange(nameSearchValue: string) {
    this.filtro.nameSearch = nameSearchValue;
    this.findLogOfProceduresExec();
  }

  onStatusChange(value: string) {
    if (value != 'Todos') {
      this.filtro.execStatusSearch = value;
    } else {
      this.filtro.execStatusSearch = null
    }
    this.findLogOfProceduresExec();
  }

  onMalhaChange(value: string) {
    if (value != 'Todos') {
      this.filtro.malhaSearch = value;
    } else {
      this.filtro.malhaSearch = null
    }
    this.findLogOfProceduresExec();
  }

  onSistemaChange(value: string) {
    if (value != 'Todos') {
      this.filtro.sistemaSearch = value;
    } else {
      this.filtro.sistemaSearch = null
    }
    this.findLogOfProceduresExec();
  }

  onSelectChange(limitValue: number) {
    this.limit = limitValue;
    this.findLogOfProceduresExec()
  }

  openInfo(ref: any, data: ProcLogExec) {
    this.infoLogModalRef = this.modalService.open(ref);
    this.infoModalData = data; 
  }

  openChart(ref: any, data: ProcLogExec) {
    this.chartModalRef = this.modalService.open(ref, {windowClass: 'modalGraphSize'});
    this.chartModalData = data.procAvailable;
  }

  refresh() {
    this.findLogOfProceduresExec();
  }

  openErrorLog(ref: any, data: ProcLogExec) {
    this.errorLogModalRef = this.modalService.open(ref, {windowClass: 'modalErrorSize' });
    this.errorModalData = data;
  }

  findLogOfProceduresExec() {
    this.lastUpdate = new Date();
    this.isBoxLoading = true;
    this.procLogExecList = null;
    this.boxLoadingError = false;

    this.proceduresService.findAllLogsPageable(this.pages, this.limit, this.orderBy, this.orderAscOrDesc, this.filtro)
      .subscribe((res: any) => {
        this.isBoxLoading = false;
        this.procLogExecPagination = res;
        this.procLogExecList = this.procLogExecPagination.content;
        delete this.procLogExecPagination.content;
        this.pagNumberOfPages = 10 * Math.ceil(this.procLogExecPagination.totalElements / this.limit);

        this.procLogExecList.map((element, index) => {
          element.procAvailable = this.proceduresService.translateProcAvailable(element.procAvailable);
          if (this.colTimerArr[index]) {
            clearInterval(this.colTimerArr[index]);
          }

          if (element) {
            if (element.tempoMedio) {
              element.tempoMedioDigital =
                SharedService.convertMillisecondsToDigitalClock(element.tempoMedio);
              element.procAvailable.tempoMedioDigital = element.tempoMedioDigital;
            }
            if (element.tempoExecucao) {
              element.tempoExecucao
                = SharedService.convertMillisecondsToDigitalClock(element.tempoExecucao);
            } else if (element.status === "R") {

              let initDate = new Date(element.dataInicio);
              let tempoEstimadoRunTime
                = SharedService.convertTimeToMilliseconds(new Date(element.procAvailable.tempoEstimado));

              this.colTimerArr[index] = setInterval(() => {
                let dateNow = new Date();
                let timeDiff = Math.abs(initDate.getTime() - dateNow.getTime());
                if (timeDiff > tempoEstimadoRunTime) {
                  element.status = "T";
                }
                element.tempoExecucao
                  = SharedService.convertMillisecondsToDigitalClock(timeDiff);
              }, 1000);
            }
          }
        })
      }, () => {
        this.isBoxLoading = false;
        this.boxLoadingError = true;
      });
  }

  populateSelects() {
    this.proceduresService.findMalhasNome()
      .subscribe((res: string[]) => {
        if (res.length > 0) {
          res.forEach(element => {
            this.malhaOptions.push({
              value: element,
              info: element
            });
          });
        }
      }, () => { });
    this.proceduresService.findSistemas()
      .subscribe((res: string[]) => {
        if (res.length > 0) {
          res.forEach(element => {
            this.sistemaOptions.push({
              value: element,
              info: element
            });
          });
        }
      })
  }
}
