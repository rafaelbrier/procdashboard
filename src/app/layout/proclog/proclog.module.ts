import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProcLogPage } from './proclog/proclog.page';
import { ProcLogRoutingModule } from './proclog.routing.module';
import { FormsModule } from '@angular/forms';
import { ErrorLoadModule } from 'src/app/components/error-load/error-load.module';
import { EmptyListModule } from 'src/app/components/empty-list/empty-list.module';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'src/app/components/modal/modal.module';
import { ProceduresService } from 'src/app/core/infra/services/procedures.service';

@NgModule({
    declarations: [
        ProcLogPage
    ],
    imports: [
        CommonModule,
        FormsModule,
        ProcLogRoutingModule,
        //ForPageUse
        ErrorLoadModule,
        EmptyListModule,
        NgbPaginationModule,
        ModalModule
    ],
    providers: [
        ProceduresService
    ]
})
export class ProcLogModule { }
