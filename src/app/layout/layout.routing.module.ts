import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
      },
      {
        path: 'home',
        loadChildren: './home/home.module#HomeModule'
      },
      {
        path: 'proclog',
        loadChildren: './proclog/proclog.module#ProcLogModule'
      },
      {
        path: 'runbook',
        loadChildren: './runbook/runbook.module#RunBookModule'
      },
      {
        path: 'agendreg',
        loadChildren: './agendreg/agendreg.module#AgendRegModule'
      },
      // {
      //   path: 'usuarios',
      //   loadChildren: './usuarios/usuarios.module#UsuariosModule'
      // },
      // {
      //   path: 'erro',
      //   loadChildren: './erros/erros.module#ErrosModule'
      // }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }