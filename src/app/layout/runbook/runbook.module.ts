import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RunBookPage } from './runbook/runbook.page';
import { FormsModule } from '@angular/forms';
import { RunBookRoutingModule } from './runbook.routing.module';
import { ErrorLoadModule } from 'src/app/components/error-load/error-load.module';
import { DropDownBoxModule } from 'src/app/components/dropdown-box/dropdown-box.module';
import { ProceduresService } from 'src/app/core/infra/services/procedures.service';
import { ShowMoreModule } from 'src/app/components/show-more/show-more.module';
import { ModalModule } from 'src/app/components/modal/modal.module';

@NgModule({
    declarations: [
        RunBookPage
    ],
    imports: [
        CommonModule,
        FormsModule,
        RunBookRoutingModule,
        //ForPageUse
        DropDownBoxModule,
        ShowMoreModule,
        ErrorLoadModule,
        ModalModule
    ],
    providers: [
        ProceduresService
    ]
})
export class RunBookModule { }
