import { Component, OnInit } from '@angular/core';
import { ProcAvailable, Malha, ProcLogExec } from 'src/app/core/model/procedure.model';
import { ProceduresService } from 'src/app/core/infra/services/procedures.service';
import { ExcelService } from 'src/app/core/infra/services/excel.service';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-runbook',
  templateUrl: './runbook.page.html',
  styleUrls: ['./runbook.page.scss']
})
export class RunBookPage implements OnInit {

  //ModalsRef
  chartModalRef: NgbModalRef;
  chartModalData: ProcLogExec;

  isBoxLoading: boolean = false;
  boxLoadingError: boolean = false;

  //Pagination
  pages: number = 0;
  pagNumberOfPages: number;
  orderBy: string = "nome";
  orderAscOrDesc: string = "asc";
  limit: number = 200;

  //Search
  malha: string = "Todos";
  malhaOptions: any[] = [{ value: "Todos", info: "Todos" }];

  sistema: string = "Todos";
  sistemaOptions: any[] = [{ value: "Todos", info: "Todos" }];

  filtro: any = {
    nameSearch: null,
    malhaSearch: null,
    sistemaSearch: null,
  }

  procAvailablePagination: any;
  procAvailableList: ProcAvailable[];
  proceduresFromMalhas: {
    RPZRESID: ProcAvailable[],
    RPZREDTS: ProcAvailable[],
  } = { RPZRESID: [], RPZREDTS: [] };
  malhas: Malha[];

  constructor(private proceduresService: ProceduresService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.populateSelects();
    this.findAllMalhasAndProcedures();
  }

  onNameSearchChange(nameSearchValue: string) {
    this.filtro.nameSearch = nameSearchValue;
    this.findAllProcedures();
  }

  onMalhaChange(value: string) {
    if (value != 'Todos') {
      this.filtro.malhaSearch = value;
    } else {
      this.filtro.malhaSearch = null;
    }
  }

  onSistemaChange(value: string) {
    if (value != 'Todos') {
      this.filtro.sistemaSearch = value;
    } else {
      this.filtro.sistemaSearch = null
    }
    this.findAllProcedures();
  }

  openChart(ref: any, data: ProcAvailable) {
    this.chartModalRef = this.modalService.open(ref, {windowClass: 'modalGraphSize'});
    this.chartModalData = data;
 }

  exportToExcel(procedures: ProcAvailable[], malha: string) {
    let dataCopy = ExcelService.formatProcToCSV(procedures);
    ExcelService.exportToCSVFile(dataCopy, "Proc_" + malha);
  }

  exportAllToExcel() {
    let malhaKeys = Object.keys(this.proceduresFromMalhas);
    let dataCopy = [];
    malhaKeys.forEach(key => {
      dataCopy = dataCopy.concat(this.proceduresFromMalhas[key]);
    });
    dataCopy = ExcelService.formatProcToCSV(dataCopy);
    ExcelService.exportToCSVFile(dataCopy, "Procedimentos");
  }

  separateProcedures() {
    let malhaKeys = Object.keys(this.proceduresFromMalhas);
     malhaKeys.forEach(key => {
     this.proceduresFromMalhas[key]
       = this.procAvailableList.filter(obj => {return obj.malhaNome == key});
     this.procAvailableList
       = this.procAvailableList.filter(obj => {return obj.malhaNome != key});
    });
  }

  findAllMalhasAndProcedures() {
    this.isBoxLoading = true;
    this.boxLoadingError = false;

    this.proceduresService.findAllMalhas()
    .subscribe((res: Malha[]) => {
      this.isBoxLoading = false;
      this.malhas = res;
      this.findAllProcedures();
    }, () => {
      this.isBoxLoading = false;
      this.boxLoadingError = true;
    });
  }

  findAllProcedures() {
    this.isBoxLoading = true;
    this.boxLoadingError = false;

    this.proceduresService.findAllProceduresPageable(this.pages,
      this.limit, this.orderBy, this.orderAscOrDesc, this.filtro)
      .subscribe((res: ProcAvailable[]) => {
        this.isBoxLoading = false;
        this.procAvailableList = res;
        this.procAvailableList.map(element => {
          element = this.proceduresService.translateProcAvailable(element);
        })
        this.separateProcedures();

      }, () => {
        this.isBoxLoading = false;
        this.boxLoadingError = true;
      });
  }

  populateSelects() {
    this.proceduresService.findMalhasNome()
    .subscribe((res: string[]) => {
        if(res.length > 0) {
          res.forEach(element => {
            this.malhaOptions.push({
              value: element,
              info: element
            });
          });
        }
    }, () => {});
    this.proceduresService.findSistemas()
    .subscribe((res: string[]) => {
      if(res.length > 0) {
        res.forEach(element => {
          this.sistemaOptions.push({
            value: element,
            info: element
          });
        });
      }
    })
  }

  getColor(i: number) {
    i = (i % 10);
    switch (i) {
      case 0: return '#f0fbff';
      case 1: return '#f3fff0';
      case 2: return '#f4f0ff';
      case 3: return '#fefff0';
      case 4: return '#f5f0ff';
      case 5: return '#f0f8ff';
      case 6: return '#f1fff0';
      case 7: return '#fff0f0';
      case 8: return '#f6fff0';
      case 9: return '#fffbf0';
    }
  }

}
