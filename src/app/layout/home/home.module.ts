import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home.routing.module';
import { DashboardPage } from './dashboard/dashboard.page';
import { MyChartModule } from 'src/app/components/chart/chart.module';

@NgModule({
    declarations: [
        DashboardPage,
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        MyChartModule
    ],
    providers: [
    ]
})
export class HomeModule { }
