import { NgModule } from '@angular/core';
import { LayoutRoutingModule } from './layout.routing.module';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { TemplateModule } from '../core/template/template.module';

@NgModule({
    declarations: [
        LayoutComponent
    ],
    imports: [
        CommonModule,
        LayoutRoutingModule,
        //ForPagesUse
        TemplateModule
    ],
    providers: [ ],
    
})
export class LayoutModule { }
