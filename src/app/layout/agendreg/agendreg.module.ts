import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgendRegPage } from './agendreg/agendreg.page';
import { AgendRegRoutingModule } from './agendreg.routing.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { ProceduresService } from 'src/app/core/infra/services/procedures.service';

@NgModule({
    declarations: [
        AgendRegPage,
    ],
    imports: [
        CommonModule,
        AgendRegRoutingModule,
        //forPageUse
        MatExpansionModule
    ],
    providers: [
        ProceduresService
        ]
})
export class AgendRegModule { }
