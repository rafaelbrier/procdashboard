import { OnInit, Component } from '@angular/core';
import { ProceduresService } from 'src/app/core/infra/services/procedures.service';
import { ProcAvailable } from 'src/app/core/model/procedure.model';

@Component({
    selector: 'app-agendreg',
    templateUrl: './agendreg.page.html',
    styleUrls: ['./agendreg.page.scss']
  })
  export class AgendRegPage implements OnInit {

    dateArr: Date[] = [];
    //procedures: ProcAvailable[];
    //logExecLastMonth: ProcLogExec[];
    procExecIdArr: String[] = [];

    constructor(private proceduresService: ProceduresService) {}

    ngOnInit() {
      this.populateDateArr();
      this.findAllProcedures();
    }

    populateDateArr() {
      for(let i = 0; i < 7; i++) {
        this.dateArr.push(new Date(new Date().setDate(new Date().getDate() - i)));
      }
    }
  
    processExecutionPeriod(procs: ProcAvailable[]) {
      procs.forEach(element => {
          if(element.periodicidade === "D") {
            
          }
      });
    }

    findAllProcedures() {
      this.proceduresService.findAll()
      .subscribe((res: ProcAvailable[]) => {
        this.processExecutionPeriod(res);
      })
    }
  }