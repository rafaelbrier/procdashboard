import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgendRegPage } from './agendreg/agendreg.page';

const routes: Routes = [
    {
        path: '',
        component: AgendRegPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AgendRegRoutingModule { }