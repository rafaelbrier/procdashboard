import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown-box',
  templateUrl: './dropdown-box.component.html',
  styleUrls: ['./dropdown-box.component.scss']
})
export class DropdownBoxComponent implements OnInit {

  @Input() title: string;
  @Input() description: string;
  @Input() bgColor: string;

  constructor() { }

  ngOnInit() {
  }

}
