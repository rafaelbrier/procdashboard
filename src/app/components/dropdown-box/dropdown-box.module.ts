import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownBoxComponent } from './dropdown-box/dropdown-box.component';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
    declarations: [
        DropdownBoxComponent
    ],
    imports: [
        CommonModule,
        MatExpansionModule
    ],
    exports: [
        DropdownBoxComponent
    ],
    providers: [],
})
export class DropDownBoxModule { }