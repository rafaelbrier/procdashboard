import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorLoadComponent } from './error-load/error-load.component';

@NgModule({
    declarations: [
        ErrorLoadComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ErrorLoadComponent
    ]
})
export class ErrorLoadModule { }