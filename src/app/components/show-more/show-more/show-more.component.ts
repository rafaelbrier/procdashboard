import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-show-more',
  template: `
      <div>
          {{text}}
        <span *ngIf="show" (click)="changeText()" class="expand-button">
          {{textExpanded ?  ' ... menos' : ' ... mais'}}
        </span>
      </div>
  `,
  styles: [`
      .expand-button {
        cursor: pointer;
        font-weight: 500;
        color: blue;
      }
  `]
})
export class ShowMoreComponent implements OnInit {

  @Input() text: string;
  @Input() charMax: number = 50;

  textOrigin: string;
  textCropped: string;
  show: boolean = false;
  textExpanded: boolean = false;

  constructor() { }

  ngOnInit() {
    this.checkTextSize();
  }

  checkTextSize() {
    this.textOrigin = this.text;
    if (this.text) {
      if (this.text.length > this.charMax) {
        this.textCropped = this.text.substr(0, this.charMax);
        this.text = this.textCropped;
        this.show = true;
      } else {
        this.text = this.textOrigin;
        this.show = false;
      }
    }
  }

  changeText() {
    this.textExpanded = !this.textExpanded;
    if(this.textExpanded) {
      this.text = this.textOrigin;
    } else {
      this.text = this.textCropped;
    }
  }

}
