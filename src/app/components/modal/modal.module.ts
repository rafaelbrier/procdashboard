import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ShowMoreModule } from 'src/app/components/show-more/show-more.module';
import { MyChartModule } from 'src/app/components/chart/chart.module';
import { ChartModalComponent } from './chart/chart.modal';
import { ErrorLogModalComponent } from './error-log/error-log.modal';
import { InfoLogModalComponent } from './info-log/info-log.modal';
import { ErrorLoadModule } from '../error-load/error-load.module';
import { EmptyListModule } from '../empty-list/empty-list.module';
import { ErrorService } from 'src/app/core/infra/services/error.service';

@NgModule({
    declarations: [
        ChartModalComponent,
        ErrorLogModalComponent,
        InfoLogModalComponent
    ],
    imports: [
        CommonModule,
        NgbModule,
        ShowMoreModule,
        MyChartModule,
        ErrorLoadModule,
        EmptyListModule
    ],
    exports: [
        ChartModalComponent,
        ErrorLogModalComponent,
        InfoLogModalComponent
    ],
    providers: [
        NgbActiveModal,
        ErrorService
    ],
    entryComponents: []
})
export class ModalModule { }