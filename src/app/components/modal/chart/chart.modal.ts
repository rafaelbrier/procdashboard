import { Component, Input, OnInit } from '@angular/core';
import { ModalModel } from 'src/app/core/model/modal.model';
import { ProcAvailable } from 'src/app/core/model/procedure.model';
import { GraphChart, ProcChartData } from 'src/app/core/model/chart.model';
import { Chart } from 'angular-highcharts';
import { ProceduresService } from 'src/app/core/infra/services/procedures.service';
import { SharedService } from 'src/app/core/infra/services/shared.service';
import { ChartService } from 'src/app/core/infra/services/chart.service';

@Component({
    selector: 'modal-chart',
    templateUrl: './chart.modal.html',
    styleUrls: ['../modal.scss']
  })
export class ChartModalComponent extends ModalModel implements OnInit {
    @Input()
    data: ProcAvailable;

    chartExec: Chart;
    chartError: Chart;
    tempoMedio: string;
    disponibilidade: number;
    numExec: number;
    areaChartData: ProcChartData[];
   
    constructor(private proceduresService: ProceduresService) {
        super();
    }

    ngOnInit() {
      
      this.isBoxLoading = true;
      this.boxLoadingError = false;

      this.proceduresService.findChartData(this.data.procId)
      .subscribe((res: ProcChartData[]) => {
        let chartDataArr = res;
        this.elementCount = chartDataArr.length;
        this.isBoxLoading = false;
        chartDataArr.forEach(element => {
          element.tempoEstimado
          = SharedService.convertTimeToMilliseconds(new Date(this.data.tempoEstimado));
        });
          this.evaluateGraph(chartDataArr)
      }, () => { 
        this.isBoxLoading = false;
        this.boxLoadingError = true;
      })
    }

    evaluateGraph(areaChartData: ProcChartData[]) {
      let charts: GraphChart = ChartService.plotModalGraphs(areaChartData);
      this.chartExec = charts.chartExec;
      this.chartError = charts.chartError;
      this.tempoMedio = charts.tempoMedio;
      this.disponibilidade = charts.disponibilidade;
      this.numExec = charts.numExec;  
    }
   
}