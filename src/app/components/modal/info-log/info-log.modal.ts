import { Component, Input, OnInit } from '@angular/core';
import { ModalModel } from 'src/app/core/model/modal.model';
import { ProcLogExec, ProcAvailable } from 'src/app/core/model/procedure.model';

@Component({
    selector: 'modal-info-log',
    templateUrl: './info-log.modal.html',
    styleUrls: ['../modal.scss']
  })
export class InfoLogModalComponent extends ModalModel implements OnInit {
    @Input()
    data: ProcLogExec;

    procedureData: ProcAvailable;
   
    constructor() {
        super();
    }

    ngOnInit() {
      this.procedureData = this.data.procAvailable;
    }
   
}