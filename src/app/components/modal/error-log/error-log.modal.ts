import { Component, Input, OnInit } from '@angular/core';
import { ProcLogError } from 'src/app/core/model/error.model';
import { ModalModel } from 'src/app/core/model/modal.model';
import { ErrorService } from 'src/app/core/infra/services/error.service';
import { ProcLogExec } from 'src/app/core/model/procedure.model';

@Component({
    selector: 'modal-error-log',
    templateUrl: './error-log.modal.html',
    styleUrls: ['../modal.scss']
})
export class ErrorLogModalComponent extends ModalModel implements OnInit {
    @Input()
    data: ProcLogExec;

    private errorList: ProcLogError[];

    constructor(private errorService: ErrorService) {
        super();
    }

    ngOnInit() {
        this.isBoxLoading = true;
        this.boxLoadingError = false;

        this.errorService.findErrorsOfExec(this.data.logExecId,
            (response: ProcLogError[]) => {
                this.errorList = response;
                this.elementCount = this.errorList.length;
                this.isBoxLoading = false;
            }, () => {
                this.isBoxLoading = false;
                this.boxLoadingError = true;
            })
    }

}