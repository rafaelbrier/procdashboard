// chart.component.ts
import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'angular-highcharts';
 
@Component({
  selector: 'app-chart',
  template: `
    <div [chart]="chart"></div>
  `
})
export class ChartComponent implements OnInit {

  @Input() chart: Chart;
  constructor() {}
  ngOnInit() {}
}