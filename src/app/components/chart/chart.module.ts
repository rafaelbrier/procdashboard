import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartComponent } from './chart/chart.component';
import { ChartModule } from 'angular-highcharts';

@NgModule({
    declarations: [
        ChartComponent
    ],
    imports: [
        CommonModule,
        ChartModule
    ],
    exports: [
        ChartComponent
    ]
})
export class MyChartModule { }