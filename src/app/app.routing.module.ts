import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    {
      path: '',
      pathMatch: 'full',
      redirectTo: ''
    },
    {
      path: '',
      loadChildren: './layout/layout.module#LayoutModule',
    }
    // {
    //   path: 'login',
    //   loadChildren: './layout/login/login.module#LoginModule',
    // },
  ];
  
  @NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
